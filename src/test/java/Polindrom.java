public class Polindrom {
    //TODO: Verify if strings from the array are Polindrom or not
    public static void main(String[] args) {

        String[] values = {"Racecar", "Was it a cat I saw", "dad", "New string"};
        verifyIfStringPolindrom(values);
    }

    public static void verifyIfStringPolindrom(String[] values) {

        for (String value : values) {
            String reverse = "";
            for (int x = value.length() - 1; x >= 0; x--) {
                reverse = reverse + value.charAt(x);

            }

            if (value.equalsIgnoreCase(reverse)) {
                System.out.println(value + " - This string is Polindrom");
            } else {
                System.out.println(value + " - This string is not Polindrom");
            }

        }

    }
}
